# FactoryX_Checkout

Documentation: https://bitbucket.org/factoryx-developers/factoryx_checkout/wiki/

## RELEASE NOTES

### 0.1.4
- Add coupon code + message to config

### 0.1.3
- Fix naming of config field

### 0.1.2
- Handle hospital field

### 0.1.1
- Add uninstall script for https://github.com/magento-hackathon/MageTrashApp

### 0.1.0
- Improvement: avoid rewriting existing method, create a new method instead as there is no way for this feature to work without block rewrite: https://bitbucket.org/factoryx-developers/factoryx_checkout/issues/1/try-to-avoid-block-rewrites

### 0.0.3
- Switch to modman

### 0.0.2
- Initial release