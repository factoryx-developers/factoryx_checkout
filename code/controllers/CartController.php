<?php
require_once  Mage::getModuleDir('controllers', 'Mage_Checkout').DS.'CartController.php';

/**
 * Class FactoryX_Checkout_CartController
 */
class FactoryX_Checkout_CartController extends Mage_Checkout_CartController
{
    public function hospitalPostAction()
    {
        $workLocation = (string) $this->getRequest()->getParam('hospital_clinic_work_location');
        $couponCode = Mage::helper('fx_checkout')->getCheckCartCouponCode();

        if (
            !strlen($workLocation)
            ||
            !$couponCode
        ) {
            $session = Mage::getSingleton('checkout/session');
            $session->setWorkLocation("");
            if ($this->_getQuote()->getCouponCode() == $couponCode) {
                Mage::helper('fx_checkout')->log(sprintf("%s->removeCode: %s", __METHOD__, $couponCode) );
                $this->_setCouponCode('');
            }
            $this->_getSession()->addError($this->__('No hospital or clinic entered.'));
            $this->_goBack();
            return;
        }

        try {
            $session = Mage::getSingleton('checkout/session');
            $session->setWorkLocation($workLocation);
            $this->_setCouponCode($couponCode);
            Mage::helper('fx_checkout')->log(sprintf("%s->setCouponCode: %s|%s", __METHOD__, $couponCode, $workLocation) );
            $successMessage = Mage::helper('fx_checkout')->getCheckCartSuccessMessage();
            if (empty($successMessage)) {
                $successMessage = $this->__('Thank-you for entering your Hospital/Clinic "%s"', $workLocation);
            }
            $this->_getSession()->addSuccess($successMessage);
        }
        catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        }
        catch (Exception $e) {
            $this->_getSession()->addError($this->__('Cannot apply the coupon code.'));
            Mage::logException($e);
        }

        $this->_goBack();
    }

    /**
     * _setCouponCode
     *
     * @param string $couponCode
     */
    private function _setCouponCode($couponCode = '') {
        $this->_getQuote()->setCouponCode($couponCode);
        $this->_getQuote()->getShippingAddress()->setCollectShippingRates(true);
        $this->_getQuote()->collectTotals()->save();
    }
}