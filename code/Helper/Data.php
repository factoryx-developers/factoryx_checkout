<?php
/**
 * class FactoryX_Checkout_Helper_Data
 *
 * Mage::helper('fx_checkout')
 */
class FactoryX_Checkout_Helper_Data extends Mage_Core_Helper_Abstract
{
    protected $logFileName = 'factoryx_checkout.log';

    /**
     * config
     */
    const XML_CONFIG_CHECKOUT_CART_HOSPITAL = "checkout/cart/hospital";

    const XML_CONFIG_CHECKOUT_CART_COUPON_CODE = "checkout/cart/coupon_code";

    const XML_CONFIG_CHECKOUT_CART_SUCCESS_MESSAGE = "checkout/cart/success_message";

    /**
     * Log data
     * @param string|object|array data to log
     */
    public function log($data)
    {
        Mage::log($data, null, $this->logFileName);
    }

    /**
     * isCheckCartHospitalEnabled
     *
     * @return bool
     */
    public function isCheckCartHospitalEnabled() {
        return Mage::getStoreConfigFlag(self::XML_CONFIG_CHECKOUT_CART_HOSPITAL) && !empty(Mage::getStoreConfigFlag(self::XML_CONFIG_CHECKOUT_CART_COUPON_CODE));
    }

    /**
     * getCheckCartCouponCode
     *
     * @return string
     */
    public function getCheckCartCouponCode() {
        return Mage::getStoreConfig(self::XML_CONFIG_CHECKOUT_CART_COUPON_CODE);
    }

    /**
     * getCheckCartSuccessMessage
     *
     * @return string
     */
    public function getCheckCartSuccessMessage() {
        return Mage::getStoreConfig(self::XML_CONFIG_CHECKOUT_CART_SUCCESS_MESSAGE);
    }
}